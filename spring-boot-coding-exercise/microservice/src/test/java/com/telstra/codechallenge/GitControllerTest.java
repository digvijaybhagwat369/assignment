package com.telstra.codechallenge;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.SortingFocusTraversalPolicy;

import org.json.JSONException;
/*import org.junit.jupiter.api.Test;*/
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.client.RestClientException;
import org.springframework.web.context.WebApplicationContext;

import com.telstra.codechallenge.assignment.GitService;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = MicroServiceMain.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GitControllerTest  {
	  
	  @LocalServerPort
	  private int port;

	  @Autowired
	  private TestRestTemplate restTemplate;
	  
	  @Autowired
	   WebApplicationContext webApplicationContext;
	  
	  @Autowired
	  GitService gitService;
	  	  
	
	  HttpHeaders headers = new HttpHeaders();
	  
	  protected MockMvc mvc;
	  
	  @Test
	  public void testGetGitUser() throws RestClientException,JSONException {
		  
		  HttpEntity<String> entity = new HttpEntity<String>(null, headers);

			ResponseEntity<String> response = restTemplate.exchange(
					createURLWithPort("/getgituser"),
					HttpMethod.GET, entity, String.class);

			String expected = "[{\"id\":\"68911683\",\"html_url\":\"https://github.com/daniel-e/tetros\",\"login\":\"daniel-e\"}]";

			System.out.println("body----"+response.getBody());
			
			JSONAssert.assertEquals(expected, response.getBody(), false);
		  
	  }
	  
	  private String createURLWithPort(String uri) {
			return "http://localhost:" + port + uri;
		}
	  

}
