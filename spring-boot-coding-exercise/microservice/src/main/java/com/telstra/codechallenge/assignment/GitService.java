package com.telstra.codechallenge.assignment;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class GitService {

	@Autowired
	private RestTemplate restTemplate;
	
	

	@Value("${git.getuser.url}")
	private String url;
	
	public List<Items> getGitUser(String q, String sort,String order,int record)
	{
		String newUrl= url+q+"&sort="+sort+"&order="+order;
		 
		Response response = restTemplate.getForObject(newUrl, Response.class) ;
		 
		UtilityFun fun = (inputResponse , inputRecord) -> response.getItems().stream().limit(record).collect(Collectors.toList());
	
		return fun.getFormatedList(response, record);
				 
	}
	
}
