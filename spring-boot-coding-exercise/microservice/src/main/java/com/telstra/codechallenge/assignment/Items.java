package com.telstra.codechallenge.assignment;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonIgnoreProperties(ignoreUnknown = true)
public class Items {

	private String id;
	
	@JsonIgnore
	private String name;
	
	@JsonProperty("login")
	public String getLogin() {
		return owner.getLogin();
	}
		
	private String html_url;
	
	
	private Owner owner;
	

	public Items() {
		
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHtml_url() {
		return html_url;
	}

	public void setHtml_url(String html_url) {
		this.html_url = html_url;
	}

	

	public void setOwner(Owner owner) {
		this.owner = owner;
	}
		
    

	
	
	@Override
	public String toString() {
		return "Items [id=" + id + ", name=" + name + ", html_url=" + html_url + ", owner=" + owner + "]";
	}

	
	
	

	
	
	
	
}
