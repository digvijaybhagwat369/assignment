package com.telstra.codechallenge.assignment;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.web.bind.annotation.RestController;




@RestController
public class GitController {
	
	@Autowired
	private GitService gitService;
	
	
    
	
	  @RequestMapping(path = "/getgituser", method = RequestMethod.GET)
	  public List<Items> hello(@RequestParam(value = "q", defaultValue = "tetris+language:assembly") String qvalue,
			  @RequestParam(value = "sort", defaultValue = "stars") String sort,
			  @RequestParam(value = "order", defaultValue = "desc") String order,
			  @RequestParam(value = "record", defaultValue = "1") int record ) {
		  
		  List<Items> response = gitService.getGitUser(qvalue,sort,order,record);
			         
		     return response;
		   
		  }
	


}
