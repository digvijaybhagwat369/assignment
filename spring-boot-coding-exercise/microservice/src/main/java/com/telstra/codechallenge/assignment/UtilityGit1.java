package com.telstra.codechallenge.assignment;

import java.util.List;
import java.util.stream.Collectors;


public class UtilityGit1 {

	
	
	public static List<Items> getFormatedList(Response response,int record)
	{		
		return response.getItems()
	            .stream()
	            .limit(record)
	            .collect(Collectors.toList());
		

	}
	
	
	
}
